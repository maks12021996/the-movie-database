import { Fragment, useEffect } from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import "./App.css";
import { useSelector, useDispatch } from "react-redux";
import { Header } from "./components/header";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Main } from "./pages/main";
import { Details } from "./pages/movieDetails";
import { Favorites } from "./pages/favorites";
import { Wrapper } from "./components/containers";
import { NotFound } from "./pages/notFound";
import Api from "./api";
import { onReady, onSetGanres } from "./store/action";
import { formatGanres } from "./helpers";

function App() {
  const dispatch = useDispatch();
  const isAppReady = useSelector((store) => store.isAppReady);

  useEffect(() => {
    if (!localStorage.getItem("favorites"))
      localStorage.setItem("favorites", JSON.stringify([]));
    Api.getGanres().then((res) => {
      dispatch(onSetGanres(formatGanres(res)));
      dispatch(onReady());
    });
    // eslint-disable-next-line
  }, []);
  if (!isAppReady)
    return (
      <div className="center">
        <CircularProgress color="secondary" />
      </div>
    );

  return (
    <Fragment>
      <Header />
      <Wrapper>
        <Routes>
          <Route exact path="/" element={<Navigate replace to="/movies/1" />} />
          <Route path="/movies/:page" element={<Main />} />
          <Route path="/details/:id" element={<Details />} />
          <Route path="/favorites" element={<Favorites />} />
          <Route element={NotFound} />
        </Routes>
      </Wrapper>
    </Fragment>
  );
}

export default App;
