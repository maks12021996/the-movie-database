import { Link } from "react-router-dom";

export const NotFound = () => {
  return (
    <div>
      <h3 style={{ color: "#fff" }}>No Found</h3>
      <Link to="/movies/1">Main</Link>
    </div>
  );
};
