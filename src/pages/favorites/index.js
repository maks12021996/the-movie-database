import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import CircularProgress from "@material-ui/core/CircularProgress";

import Api from "../../api";
import { MovieItem } from "../../components/movieItem";
import { MovieList } from "../../components/containers";

export const Favorites = () => {
  const favorites = useSelector((s) => s.favorites);
  const movieDetails = useSelector((s) => s.movieDetails);
  const storedId = Object.keys(movieDetails);
  const [list, setList] = useState(null);

  useEffect(() => {
    const stored = favorites.filter((id) => storedId.includes(id + ""));
    const unstored = favorites.filter((id) => !storedId.includes(id + ""));
    Promise.all(unstored.map((id) => Api.getDetails(id))).then((responses) => {
      const _list = [...responses];
      stored.forEach((id) => _list.push(movieDetails[id]));
      setList(_list);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderItems = () =>
    list.map((movie) => <MovieItem key={movie.id} movie={movie} />);

  if (!list)
    return (
      <div className="center">
        <CircularProgress color="secondary" />
      </div>
    );

  return <MovieList>{renderItems()}</MovieList>;
};
