import { useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import Api from "../../api";
import ReactPaginate from "react-paginate";
import { MovieItem } from "../../components/movieItem";
import { MovieList } from "../../components/containers";
import { useSelector, useDispatch } from "react-redux";
import { onSetPage } from "../../store/action";
import { CircularProgress } from "@material-ui/core";

export const Main = () => {
  const dispatch = useDispatch();
  const { movies, totalPages } = useSelector((s) => s);
  const { page } = useParams();
  const history = useNavigate();

  useEffect(() => {
    if (!movies[page]) {
      Api.getNowPlaying(page).then(({ results, total_pages }) => {
        dispatch(onSetPage(page, results, total_pages));
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const renderItems = () =>
    movies[page].map((movie) => <MovieItem key={movie.id} movie={movie} />);

  const onPageChange = (event) => {
    history(`/movies/${event.selected + 1}`);
  };

  return (
    <>
      <ReactPaginate
        className="paginator"
        breakLabel="..."
        nextLabel=">"
        onPageChange={onPageChange}
        pageRangeDisplayed={3}
        pageCount={totalPages}
        previousLabel="<"
        renderOnZeroPageCount={null}
      />
      <MovieList>
        {movies[page] ? (
          renderItems()
        ) : (
          <div className="center">
            <CircularProgress color="secondary" />
          </div>
        )}
      </MovieList>
    </>
  );
};
