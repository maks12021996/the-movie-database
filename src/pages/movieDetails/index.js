import { useParams } from "react-router-dom";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Api from "../../api";
import { onSetDetails } from "../../store/action";
import { CircularProgress } from "@material-ui/core";

export const Details = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const details = useSelector((s) => s.movieDetails[id]);
  const history = useNavigate();

  useEffect(() => {
    if (!details) {
      Api.getDetails(id).then((r) => dispatch(onSetDetails(id, r)));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  if (!details)
    return (
      <div className="center">
        <CircularProgress color="secondary" />
      </div>
    );

  return (
    <div>
      Details
      <p> Movie id: {id} </p>
      <button onClick={() => history(-1)}>Back</button>
      <img src={Api.poster_url + details.backdrop_path} alt="img" />
      <div>{JSON.stringify(details)}</div>
    </div>
  );
};
