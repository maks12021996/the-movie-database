import { Link } from "react-router-dom";
import styled from "styled-components";

export const Card = styled.div`
  background: linear-gradient(to bottom, #2b3943, #1a242b);
  border-radius: 3px;
  width: calc(25% - 10px);
  height: 500px;
  margin-bottom: 20px;
  p,
  div.additional-info {
    font-size: 12px;
    color: #939da7;
    height: 70px;
    overflow: hidden;
  }
  div.additional-info {
    height: 15px;
  }
`;
export const CardHeader = styled.div`
  background: linear-gradient(
    90deg,
    rgba(54, 79, 107, 0.3) 0%,
    rgba(55, 176, 111, 0.3) 89.98%
  );
  width: 100%;
  height: 50px;
  padding: 15px;
  display: flex;
  align-items: center;
  justify-content: space-between;

  > h3 {
    margin: 0;
    color: #fff;
    font-size: 14px;
  }
  svg {
    width: 24px;
    height: 24px;
    cursor: pointer;
    path {
      fill: ${(props) => (props.isFavorite ? "gold" : "#fff")};
    }
  }
`;
export const Poster = styled.img`
  width: 100%;
  height: 200px;
  object-fit: cover;
  margin-bottom: 20px;
`;
export const ButtonLink = styled(Link)`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  border: none;
  cursor: pointer;
  padding: 14px 32px;
  font-size: 12px;
  background: #37b06f;
  color: white;
  text-align: center;
  text-transform: uppercase;
  font-weight: 500;
  letter-spacing: 0.05em;
  line-height: 12px;
  border-radius: 3px;
  height: 44px;
  transition: all 0.3s cubic-bezier(0.165, 0.84, 0.44, 1);
  width: 100%;
  min-width: 100px;
  text-decoration: none;
`;
export const Column = styled.div`
  width: 100%;
  padding: 0 15px 20px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 225px;
`;
