import { useMatch } from "react-router-dom";
import { Header as HeaderStyled, Nav, NavLink } from "./styled";

export const Header = () => {
  return (
    <HeaderStyled>
      <Nav>
        <NavLink is_active={useMatch("/movies/:page")} to="/movies/1">
          Movies
        </NavLink>
        <NavLink is_active={useMatch("/favorites")} to="/favorites">
          Favorites
        </NavLink>
      </Nav>
    </HeaderStyled>
  );
};
